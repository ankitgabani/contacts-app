//
//  AppDelegate.swift
//  Contacts App
//
//  Created by Gabani King on 20/08/21.
//

import UIKit
import IQKeyboardManager
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var arrContactBirthday = NSMutableArray()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        sleep(1)
        IQKeyboardManager.shared().isEnabled = true
        // Override point for customization after application launch.
        return true
    }


}

