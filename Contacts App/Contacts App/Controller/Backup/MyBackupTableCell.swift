//
//  MyBackupTableCell.swift
//  Contacts App
//
//  Created by Mac MIni M1 on 26/08/21.
//

import UIKit

class MyBackupTableCell: UITableViewCell {

    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblFilrType: UILabel!
    
    
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnDelete: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
