//
//  BackupViewController.swift
//  Contacts App
//
//  Created by Gabani King on 20/08/21.
//

import UIKit

class BackupViewController: UIViewController {
    
    @IBOutlet weak var viewBackUpNow: UIView!
    @IBOutlet weak var viewMyBackUp: UIView!
    @IBOutlet weak var topView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.viewMyBackUp.clipsToBounds = true
            self.viewMyBackUp.layer.cornerRadius = self.viewMyBackUp.frame.height / 2
            self.viewMyBackUp.layerGradient(startPoint: .topLeft, endPoint: .bottomRight, colorArray: [PRIMARY_GARDIENT_1_COLOR.cgColor, PRIMARY_GARDIENT_2_COLOR.cgColor], type: .axial)

            self.viewBackUpNow.clipsToBounds = true
            self.viewBackUpNow.layer.cornerRadius = self.viewBackUpNow.frame.height / 2
            self.viewBackUpNow.layerGradient(startPoint: .topLeft, endPoint: .bottomRight, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 25
        topView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        
        setUp()
    }
    
    //MARK:- User Functin
    func setUp()
    {
        self.view.backgroundColor = UIColor.groupTableViewBackground
    }
    // Do any additional setup after loading the view.
    
    @IBAction func clickedBackUpNow(_ sender: Any) {
    }
    
    @IBAction func clickedMyBackup(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyBackupVC") as! MyBackupVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

