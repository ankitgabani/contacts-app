//
//  OtherViewController.swift
//  Contacts App
//
//  Created by Gabani King on 20/08/21.
//

import UIKit
import Photos

class OtherViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, PHPhotoLibraryChangeObserver {
   
    
    
    //MARK:- IBOutlet
    @IBOutlet weak var viewDeleteSelected: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topView: UIView!
    
    
    var arrSelectedObject = NSMutableArray()
    
    var allPhotos : PHFetchResult<PHAsset>? = nil
    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 0.0,
                                     bottom: 0.0,
                                     right: 0.0)
    let itemsPerRow: CGFloat = 4
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0 , right: 0.0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0
        _flowLayout.minimumLineSpacing = 0
        return _flowLayout
    }
    
    //MARK:- View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PHPhotoLibrary.shared().register(self)
        
        setUpPhotos()
        
        viewDeleteSelected.isHidden = true
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.collectionView.collectionViewLayout = flowLayout
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 25
        topView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        // Do any additional setup after loading the view.
    }
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
                
    }
    
//    func photoLibraryDidChange(_ changeInstance: PHChange) {
//            // Change notifications may be made on a background queue.
//            // Re-dispatch to the main queue to update the UI.
//            // Check for changes to the displayed album itself
//            // (its existence and metadata, not its member self).
//            guard let photos = allPhotos else {return}
//
//            // Check for changes to the list of assets (insertions, deletions, moves, or updates).
//            if let changes = changeInstance.changeDetails(for: photos) {
//                // Keep the new fetch result for future use.
//                allPhotos = changes.fetchResultAfterChanges
//                if changes.hasIncrementalChanges {
//                    // If there are incremental diffs, animate them in the collection view.
//                    self.collectionView.performBatchUpdates({
//                        // For indexes to make sense, updates must be in this order:
//                        // delete, insert, reload, move
//                        if let removed = changes.removedIndexes, removed.count > 0 {
//                            print("photoLibraryDidChange: Delete at \(removed.map { IndexPath(item: $0, section:0) })")
//                            self.collectionView.deleteItems(at: removed.map { IndexPath(item: $0, section:0) })
//                        }
//                        if let inserted = changes.insertedIndexes, inserted.count > 0 {
//                            print("photoLibraryDidChange: Insert at \(inserted.map { IndexPath(item: $0, section:0) })")
//                            self.collectionView.insertItems(at: inserted.map { IndexPath(item: $0, section:0) })
//                        }
//                        if var changed = changes.changedIndexes, changed.count > 0 {
//                            print("photoLibraryDidChange: Reload at \(changed.map { IndexPath(item: $0, section:0) })")
//                            // subtract removed indices
//                            if let removed = changes.removedIndexes {
//                                changed.subtract(removed)
//                            }
//                            self.collectionView.reloadItems(at: changed.map { IndexPath(item: $0, section:0) })
//                        }
//                        changes.enumerateMoves { fromIndex, toIndex in
//                            print("photoLibraryDidChange: Move at \(IndexPath(item: fromIndex, section:0)) to \(IndexPath(item: toIndex, section:0 ))")
//                            self.collectionView.moveItem(at: IndexPath(item: fromIndex, section: 0), to: IndexPath(item: toIndex, section: 0))
//                        }
//                    })
//
//                } else {
//                    // Reload the collection view if incremental diffs are not available.
//
//                    collectionView.reloadData()
//                }
//            }
//
//        }

    
    func setUpPhotos()
    {
        /// Load Photos
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized:
                print("Good to proceed")
                let fetchOptions = PHFetchOptions()
                self.allPhotos = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
            case .denied, .restricted:
                print("Not allowed")
            case .notDetermined:
                print("Not determined yet")
            case .limited:
                print("limited")
            @unknown default:
                print("default")
            }
        }
    }
    
    //MARK:- UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return allPhotos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaCollectionCell", for: indexPath) as! MediaCollectionCell
        
        let asset = allPhotos?.object(at: indexPath.row)
        cell.imgPhoto.fetchImage(asset: asset!, contentMode: .aspectFill, targetSize: cell.imgPhoto.frame.size)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(clickeDeleteImage(sender:)), for: .touchUpInside)
        
        cell.btnSelecred.tag = indexPath.row
        cell.btnSelecred.addTarget(self, action: #selector(clickeSelectedImage(sender:)), for: .touchUpInside)
        
        if self.arrSelectedObject.contains(indexPath){
            
            cell.imgSelecred.image = UIImage(named: "ic_Select")
        }
        else{
            cell.imgSelecred.image = UIImage(named: "ic_UnSelect")
        }
        
        return cell
        
    }
    
    @objc func clickeDeleteImage(sender: UIButton)
    {
        
        let asset = allPhotos?.object(at: sender.tag)
        
        asset?.requestContentEditingInput(with: PHContentEditingInputRequestOptions(), completionHandler: { (contentEditingInput, dictInfo) in

            if asset?.mediaType == .image
            {
              if let strURL = contentEditingInput?.fullSizeImageURL?.absoluteString
              {
                 print("IMAGE URL: ", strURL)
                
                let strUrl11 = URL(string: strURL)
                
                PHPhotoLibrary.shared().performChanges( {
                    let imageAssetToDelete = PHAsset.fetchAssets(withALAssetURLs: ([strUrl11] as? [URL])!, options: nil)
                    PHAssetChangeRequest.deleteAssets(imageAssetToDelete)
                },
                    completionHandler: { success, error in
                        print("Finished deleting asset. %@", (success ? "Success" : error))
                })
              }
            }
            else
            {
              if let strURL = (contentEditingInput!.avAsset as? AVURLAsset)?.url.absoluteString
              {
                 print("VIDEO URL: ", strURL)
              }
            }
          })
//
//        PHPhotoLibrary.shared().performChanges({
//            //Delete Photo
//            PHAssetChangeRequest.deleteAssets(asset as! NSFastEnumeration)
//            },
//            completionHandler: {(success, error)in
//                NSLog("\nDeleted Image -> %@", (success ? "Success":"Error!"))
//                if(success){
//
//                }else{
//                    print("Error: \(String(describing: error))")
//                }
//        })
//
      

    }
    
    @objc func clickeSelectedImage(sender: UIButton)
    {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        if self.arrSelectedObject.contains(indexPath){
            self.arrSelectedObject.remove(indexPath)
        }
        else
        {
            self.arrSelectedObject.add(indexPath)
        }
        
        if arrSelectedObject.count > 0
        {
            self.viewDeleteSelected.isHidden = false
        }
        else
        {
            self.viewDeleteSelected.isHidden = true
        }
        
        self.collectionView.reloadItems(at: [indexPath])
    }
    
    //MARK:- Action Mehthod
    @IBAction func clickedDeleteSelected(_ sender: Any) {
    }
    
    @IBAction func clickedSelectAll(_ sender: Any)
    {

        self.arrSelectedObject.removeAllObjects()
        for index in 0..<allPhotos!.count{
            let indexPath = IndexPath(row: index, section: 0)
            self.arrSelectedObject.add(indexPath)
        }
      
        if arrSelectedObject.count > 0
        {
            self.viewDeleteSelected.isHidden = false
        }
        else
        {
            self.viewDeleteSelected.isHidden = true
        }
        
        self.collectionView.reloadData()
    }
    
}

extension UIImageView{
    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
        let options = PHImageRequestOptions()
        options.version = .original
        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, _ in
            guard let image = image else { return }
            switch contentMode {
            case .aspectFill:
                self.contentMode = .scaleAspectFill
            case .aspectFit:
                self.contentMode = .scaleAspectFit
            @unknown default:
                print("default")
            }
            self.image = image
        }
    }
}
