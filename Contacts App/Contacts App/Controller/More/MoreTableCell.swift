//
//  MoreTableCell.swift
//  Contacts App
//
//  Created by Mac MIni M1 on 26/08/21.
//

import UIKit

class MoreTableCell: UITableViewCell {

    @IBOutlet weak var imgMore: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var swicth: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
