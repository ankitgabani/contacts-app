//
//  MoreViewController.swift
//  Contacts App
//
//  Created by Gabani King on 20/08/21.
//

import UIKit

class MoreViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var arrTitle = ["Backup Reminder","Birthday Reminder","Rate Us","Privacy Policy","Contact Us","More Apps"]
    var arrDes = ["Important notification about contact backup when needed.","Important notification about contact birthday.","You like it? Rate us & share app with your friends.","Read our Privacy policy.","Send us your valuable feedback.","Check out our other apps on appstore."]
    
    var arrImg = ["icbackupreminder","icbirthday","icrateus","icprivacypolicy","icbackupemail","icmoreapps"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 25
        topView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        // Do any additional setup after loading the view.
    }
     
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MoreTableCell") as! MoreTableCell
        
        cell.lblTitle.text = arrTitle[indexPath.row]
        cell.lblDes.text = arrDes[indexPath.row]
        
        cell.imgMore.image = UIImage(named: arrImg[indexPath.row])
        
        if indexPath.row == 0
        {
            cell.swicth.isHidden = false
        }
        else if indexPath.row == 1
        {
            cell.swicth.isHidden = false
        }
        else
        {
            cell.swicth.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
