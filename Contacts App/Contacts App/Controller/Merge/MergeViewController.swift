//
//  MergeViewController.swift
//  Contacts App
//
//  Created by Gabani King on 20/08/21.
//

import UIKit

class MergeViewController: UIViewController {

    @IBOutlet weak var viewMerger: UIView!
    @IBOutlet weak var topView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
            self.viewMerger.clipsToBounds = true
            self.viewMerger.layer.cornerRadius = self.viewMerger.frame.height / 2
            self.viewMerger.layerGradient(startPoint: .topLeft, endPoint: .bottomRight, colorArray: [PRIMARY_GARDIENT_2_COLOR.cgColor, PRIMARY_GARDIENT_1_COLOR.cgColor], type: .axial)
        }
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 25
        topView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        
        setUp()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- User Functin
    func setUp()
    {
        self.view.backgroundColor = UIColor.groupTableViewBackground
    }
    

    @IBAction func clickedSatrtMerge(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicatesViewController") as! DuplicatesViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
}
