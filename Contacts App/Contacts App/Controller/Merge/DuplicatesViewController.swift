//
//  DuplicatesViewController.swift
//  Contacts App
//
//  Created by Mac MIni M1 on 26/08/21.
//

import UIKit

class DuplicatesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var arrDuplicates = ["Duplicate Contacts","Duplicate Phones","Duplicate Emails","Similar Name"]
    var arrDuplicatesImage = ["icduplicatecontacts","icduplicatephones","icaccounts","icsimilarname"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        topview.clipsToBounds = true
        topview.layer.cornerRadius = 25
        topview.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        
        setUp()
        self.tblView.contentInset = UIEdgeInsets(top: 30,left: 0,bottom: 0,right: 0)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- User Functin
    func setUp()
    {
        tblView.backgroundColor = UIColor.groupTableViewBackground
        self.view.backgroundColor = UIColor.groupTableViewBackground
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDuplicates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "ContactHomeTableCell") as! ContactHomeTableCell
        
        cell.mainView.backgroundColor = UIColor.white
        cell.lblHome.text = arrDuplicates[indexPath.row]
        cell.imgHome.image = UIImage(named: arrDuplicatesImage[indexPath.row])
        
        if indexPath.row == 0
        {
            cell.mainView.clipsToBounds = true
            cell.mainView.layer.cornerRadius = 10
            cell.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        }
        else if indexPath.row == 3
        {
            cell.mainView.clipsToBounds = true
            cell.mainView.layer.cornerRadius = 10
            cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
        }
        else
        {
            cell.mainView.clipsToBounds = true
            cell.mainView.layer.cornerRadius = 0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {

            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicateContactVC") as! DuplicateContactVC
            vc.strTitle = "Duplicate Contacts"
            let nav = UINavigationController(rootViewController: self)
            UIApplication.shared.keyWindow?.rootViewController = nav
            nav.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 1
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicateContactVC") as! DuplicateContactVC
            vc.strTitle = "Duplicate Phones"
            let nav = UINavigationController(rootViewController: self)
            UIApplication.shared.keyWindow?.rootViewController = nav
            nav.pushViewController(vc, animated: true)
            
        }
        else if indexPath.row == 2
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicateContactVC") as! DuplicateContactVC
            vc.strTitle = "Duplicate Emails"
            let nav = UINavigationController(rootViewController: self)
            UIApplication.shared.keyWindow?.rootViewController = nav
            nav.pushViewController(vc, animated: true)
            
        }
        else if indexPath.row == 3
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicateContactVC") as! DuplicateContactVC
            vc.strTitle = "Similar Names"
            let nav = UINavigationController(rootViewController: self)
            UIApplication.shared.keyWindow?.rootViewController = nav
            nav.pushViewController(vc, animated: true)
            
        }
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
