//
//  FilterVC.swift
//  Contacts App
//
//  Created by Mac MIni M1 on 04/09/21.
//

import UIKit

class FilterVC: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var strTitle = ""
    
    var arrFiltersContacts = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = strTitle
        print(strTitle)
        print(arrFiltersContacts)

        
        tblView.dataSource = self
        tblView.delegate = self
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 25
        topView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- Action Method
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrFiltersContacts.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFiltersContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "DuplicateContactCell") as! DuplicateContactCell
        
        cell.viewRound.addGradientBackground()
        
        let dicData = arrFiltersContacts[indexPath.row] as? NSDictionary
        
        let fullName = dicData?.value(forKey: "fullName") as? String
        let PhoneNumbers = dicData?.value(forKey: "PhoneNumbers") as? String
        let userImage = dicData?.value(forKey: "userImage") as? String

        cell.lblTitleName.text = fullName
        cell.lblSubTitleNumber.text = PhoneNumbers
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if strTitle == "Birthday"
        {
            let headerView = Bundle.main.loadNibNamed("BirthdayHeaderView", owner: self, options: [:])?.first as! BirthdayHeaderView
            
            let dicData = arrFiltersContacts[section] as? NSDictionary
            
            let date = dicData?.value(forKey: "BirthDay") as? String
                
            var deliveryDateString = ""

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
                        
            if let createDateStr = date {
                                
                let arrSplit1 = createDateStr.components(separatedBy: "/")
                let strYear = arrSplit1[2] as! String
                let strMonth = arrSplit1[1] as! String
                let strDay = arrSplit1[0] as! String
                
                let monthNumber = (Int)(strMonth)
                
                let fmt = DateFormatter()
                fmt.dateStyle = .medium
                let strMonthName = fmt.monthSymbols[monthNumber! - 1]
                
                deliveryDateString = String.init(format: "%@ %@ ,", strDay,strMonthName.prefix(3) as CVarArg)
            }
            
            let strDay = getDayNameBy(stringDate: date!)
            
            headerView.lblTitle.text = "\(deliveryDateString) \(strDay)"
            
            headerView.clipsToBounds = true
            headerView.layer.cornerRadius = 12
            headerView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
            return headerView
            
        }
        else
        {
            let headerView = Bundle.main.loadNibNamed("DuplicateContactView", owner: self, options: [:])?.first as! DuplicateContactView
            
            headerView.btnMerge.isHidden = true
            
            return headerView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if strTitle == ""
        {
            return 32
        }
        else
        {
            return 38
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func getDayNameBy(stringDate: String) -> String
    {
        let df  = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        let date = df.date(from: stringDate)!
        df.dateFormat = "EEEE"
        return df.string(from: date);
    }

}
