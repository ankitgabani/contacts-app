//
//  AllContactViewController.swift
//  Contacts App
//
//  Created by Gabani King on 03/09/21.
//

import UIKit
import Contacts

class AllContactViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNavi: UIView!
    
    let cellID = "cellID"
    
    var contacts = [Contact]()
    var contactsWithSections = [[Contact]]()
    let collation = UILocalizedIndexedCollation.current() // create a locale collation object, by which we can get section index titles of current locale. (locale = local contry/language)
    var sectionTitles = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.dataSource = self
        tblView.delegate = self
        
        viewNavi.clipsToBounds = true
        viewNavi.layer.cornerRadius = 25
        viewNavi.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        fetchContacts()
        
        
        // Do any additional setup after loading the view.
    }
    
    private func fetchContacts(){
        
        let store = CNContactStore()
        
        store.requestAccess(for: (.contacts)) { (granted, err) in
            if let err = err{
                print("Failed to request access",err)
                return
            }
            
            if granted {
                print("Access granted")
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let fetchRequest = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                fetchRequest.sortOrder = CNContactSortOrder.userDefault
                
                do {
                    try store.enumerateContacts(with: fetchRequest, usingBlock: { ( contact, error) -> Void in
                        
                        guard let phoneNumber = contact.phoneNumbers.first?.value.stringValue else {return}
                        self.contacts.append(Contact(givenName: contact.givenName, familyName: contact.familyName, mobile: phoneNumber))
                        
                    })
                    
                    for index in self.contacts.indices{
                        
                        print(self.contacts[index].givenName)
                        print(self.contacts[index].familyName)
                        print(self.contacts[index].mobile)
                    }
                    
                    self.setUpCollation()
                    
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }
                catch let error as NSError {
                    print(error.localizedDescription)
                }
                
                
            }else{
                print("Access denied")
            }
        }
        
    }
    
    @objc func setUpCollation(){
        let (arrayContacts, arrayTitles) = collation.partitionObjects(array: self.contacts, collationStringSelector: #selector(getter: Contact.givenName))
        self.contactsWithSections = arrayContacts as! [[Contact]]
        self.sectionTitles = arrayTitles
        
        print(contactsWithSections.count)
        print(sectionTitles.count)
    }
    //MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionTitles
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsWithSections[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MissingInformationCell") as! MissingInformationCell
        
        cell.viewRound.addGradientBackground()
        let contact = contactsWithSections[indexPath.section][indexPath.row]
        
        cell.lblTitleName.text = contact.givenName + " " + contact.familyName
        
        cell.lblcall.text = contact.mobile ?? ""
        
        if contact.givenName != ""
        {
            cell.lblFirstLater.text = String("\(contact.givenName ?? "")".prefix(1))
        }
        else if contact.familyName != ""
        {
            cell.lblFirstLater.text = String("\(contact.familyName ?? "")".prefix(1))
        } else
        {
            cell.lblFirstLater.text = "N/A"
        }
        
        //        cell.btnCheckUncheck.tag = indexPath.row
        //        cell.btnCheckUncheck.addTarget(self, action: #selector(clickedDeleteContact(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //
    //        let headerView = Bundle.main.loadNibNamed("DuplicateContactView", owner: self, options: [:])?.first as! DuplicateContactView
    //
    //        headerView.lblName.text = "A"
    //        headerView.btnMerge.isHidden = true
    //
    //        return headerView
    //    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension UILocalizedIndexedCollation {
    //func for partition array in sections
    func partitionObjects(array:[AnyObject], collationStringSelector:Selector) -> ([AnyObject], [String]) {
        var unsortedSections = [[AnyObject]]()
        
        //1. Create a array to hold the data for each section
        for _ in self.sectionTitles {
            unsortedSections.append([]) //appending an empty array
        }
        //2. Put each objects into a section
        for item in array {
            let index:Int = self.section(for: item, collationStringSelector:collationStringSelector)
            unsortedSections[index].append(item)
        }
        //3. sorting the array of each sections
        var sectionTitles = [String]()
        var sections = [AnyObject]()
        for index in 0 ..< unsortedSections.count { if unsortedSections[index].count > 0 {
            sectionTitles.append(self.sectionTitles[index])
            sections.append(self.sortedArray(from: unsortedSections[index], collationStringSelector: collationStringSelector) as AnyObject)
        }
        }
        return (sections, sectionTitles)
    }
}
@objc class Contact : NSObject {
    @objc var givenName: String!
    @objc var familyName: String!
    @objc var mobile: String!
    
    init(givenName: String, familyName: String, mobile: String) {
        self.givenName = givenName
        self.familyName = familyName
        self.mobile = mobile
    }
}
