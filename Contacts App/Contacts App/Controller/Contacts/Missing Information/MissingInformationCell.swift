//
//  MissingInformationCell.swift
//  Contacts App
//
//  Created by Mac MIni M1 on 04/09/21.
//

import UIKit

class MissingInformationCell: UITableViewCell {
    
    @IBOutlet weak var viewRound: UIView!
    @IBOutlet weak var lblFirstLater: UILabel!
    
    @IBOutlet weak var lblTitleName: UILabel!

    @IBOutlet weak var imgCheckUncheck: UIImageView!
    @IBOutlet weak var btnCheckUncheck: UIButton!
    
    @IBOutlet weak var lblcall: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
