//
//  MissingInformationVC.swift
//  Contacts App
//
//  Created by Mac MIni M1 on 04/09/21.
//

import UIKit

class MissingInformationVC: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgCheckUncheck: UIImageView!
    @IBOutlet weak var btnCheckUncheck: UIButton!
    
    @IBOutlet weak var imgTrash: UIImageView!
    @IBOutlet weak var btnTrash: UIButton!
    
    @IBOutlet weak var tblView: UITableView!
    
    var strTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = strTitle
        print(strTitle)
        
        tblView.dataSource = self
        tblView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedTrash(_ sender: Any) {
        let alert = UIAlertController(title: "Delete Contact", message: "Are you sure you want to delete 1 contacts.", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Remove", style: .destructive, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    @IBAction func clickedAllSelected(_ sender: Any)
    {
        
    }
    
    @objc func clickedDeleteContact(sender: UIButton)
    {
        
    }
    
    //MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
        }
        else
        {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "MissingInformationCell") as! MissingInformationCell
        
        cell.viewRound.addGradientBackground()
        
        cell.btnCheckUncheck.tag = indexPath.row
        cell.btnCheckUncheck.addTarget(self, action: #selector(clickedDeleteContact(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("DuplicateContactView", owner: self, options: [:])?.first as! DuplicateContactView
        
        headerView.lblName.text = "A"
        headerView.btnMerge.isHidden = true
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
}
