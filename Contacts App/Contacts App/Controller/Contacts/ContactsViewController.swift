//
//  ContactsViewController.swift
//  Contacts App
//
//  Created by Gabani King on 20/08/21.
//

import UIKit
import Contacts

class ContactsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNavi: UIView!
    
    var arrHeaderName = ["General","Duplicates","Missing Information","Filters"]
    
    var arrGeneral = ["All Contacts","Accounts"]
    var arrDuplicates = ["Duplicate Contacts","Duplicate Phones","Duplicate Emails","Similar Name"]
    var arrMissing = ["No Name","No Phones","No Phones & Emails","No Group"]
    var arrFilters = ["Recently Addedd","Company","Job Title","Birthday"]
    
    var arrGeneralImage = ["icallcontacts","icaccounts"]
    var arrDuplicatesImage = ["icduplicatecontacts","icduplicatephones","icaccounts","icsimilarname"]
    var arrMissingImage = ["icnoname","icnophones","icnophoneemail","icnogroup"]
    var arrFiltersImage = ["icrecentlyadded","iccompany","icjobtitle","icbirthday"]
    
    var arrGeneralCount = NSMutableArray()
    
    var arrAllContacts = [CNContact]()
    
    var arrBirthdayContacts = NSMutableArray()
    var arrJobTitleContacts = NSMutableArray()
    var arrCompanyNameContacts = NSMutableArray()

    
    let store = CNContactStore()
    let keys = [CNContactBirthdayKey as CNKeyDescriptor]
    let contactStore = CNContactStore()

    
    //MARK:- View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.dataSource = self
        tblView.delegate = self
        
        viewNavi.clipsToBounds = true
        viewNavi.layer.cornerRadius = 25
        viewNavi.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        
        setUp()
        setGetContact()
        BithDateContacts()
        
        NotificationCenter.default.addObserver(self, selector: #selector(displayBirthdayContacts(noti:)), name: NSNotification.Name(rawValue: "ReloadBirthday"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(displayJobTitleContacts(noti:)), name: NSNotification.Name(rawValue: "ReloadJobTitle"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(displayCompanyNameContacts(noti:)), name: NSNotification.Name(rawValue: "ReloadCompanyName"), object: nil)
        
       //z3 self.allDuplicateContats()
        
         // Do any additional setup after loading the view.s
    }
    
    @objc func displayBirthdayContacts(noti:NSNotification){
        
        print(noti.object)
        arrBirthdayContacts = noti.object as! NSMutableArray
        self.tblView.reloadData()
    }
    
    @objc func displayJobTitleContacts(noti:NSNotification){
        
        print(noti.object)
        arrJobTitleContacts = noti.object as! NSMutableArray
        self.tblView.reloadData()
    }
    
    @objc func displayCompanyNameContacts(noti:NSNotification){
        
        print(noti.object)
        arrCompanyNameContacts = noti.object as! NSMutableArray
        self.tblView.reloadData()
    }
    
//    func allDuplicateContats(){
//
//        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)]
//        let request = CNContactFetchRequest(keysToFetch: keys)
//        var contactsByName = [String: [CNContact]]()
//
//        do {
//            try self.contactStore.enumerateContacts(with: request) { contact, stop in
//                guard let name = CNContactFormatter.string(from: contact, style: .fullName) else { return }
//                contactsByName[name] = (contactsByName[name] ?? []) + [contact]   // or in Swift 4, `contactsByName[name, default: []].append(contact)`
//
//            }
//        } catch let err {
//            print("error:", err)
//        }
//
//        let duplicates = contactsByName.filter { $1.count > 1 }
//
//        print(duplicates)
//    }
//
//
    //MARK:- User Functin
    func setUp()
    {
        tblView.backgroundColor = UIColor.groupTableViewBackground
        self.view.backgroundColor = UIColor.groupTableViewBackground
    }
    
    func setGetContact()
    {
        
        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)]
        let request = CNContactFetchRequest(keysToFetch: keys)
        
        let contactStore = CNContactStore()
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                self.arrAllContacts.append(contact)
            }
        }
        catch {
            print("unable to fetch contacts")
        }
    }
    
    func BithDateContacts()
    {
        
       let comman = CommanClass()
        comman.contactsDetailsFromAddressBook()

       
    }
    
    func fetchContacts() -> [CNContact]? {
        
        let containerId = store.defaultContainerIdentifier()
        let predicate = CNContact.predicateForContactsInContainer(withIdentifier: containerId)


        do {
            return try store.unifiedContacts(matching: predicate, keysToFetch: keys)
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrHeaderName.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return arrGeneral.count
        }
        else if section == 1
        {
            return arrDuplicates.count
        }
        else if section == 2
        {
            return arrMissing.count
        }
        else
        {
            return arrFilters.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "ContactHomeTableCell") as! ContactHomeTableCell
        
        if indexPath.section == 0
        {
            cell.lblHome.text = arrGeneral[indexPath.row]
            cell.imgHome.image = UIImage(named: arrGeneralImage[indexPath.row])
            
            if indexPath.row == 0
            {
                cell.lblCount.text = "\(arrAllContacts.count)"
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
            }
            else
            {
                cell.lblCount.text = "0"
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
            }
        }
        else if indexPath.section == 1
        {
            cell.lblHome.text = arrDuplicates[indexPath.row]
            cell.imgHome.image = UIImage(named: arrDuplicatesImage[indexPath.row])
            
            
            if indexPath.row == 0
            {
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
            }
            else if indexPath.row == 1
            {
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 0
                
            }
            else if indexPath.row == 2
            {
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 0
            }
            else if indexPath.row == 3
            {
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
            }
           
        }
        else if indexPath.section == 2
        {
            cell.lblHome.text = arrMissing[indexPath.row]
            cell.imgHome.image = UIImage(named: arrMissingImage[indexPath.row])
            
            
            if indexPath.row == 0
            {
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
            }
            else if indexPath.row == 1
            {
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 0
                
            }
            else if indexPath.row == 2
            {
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 0
            }
            else if indexPath.row == 3
            {
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
            }
            
        }
        else if indexPath.section == 3
        {
            cell.lblHome.text = arrFilters[indexPath.row]
            cell.imgHome.image = UIImage(named: arrFiltersImage[indexPath.row])
            
            if indexPath.row == 0
            {
                cell.lblCount.text = "0"
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
            }
            else if indexPath.row == 1
            {
                cell.lblCount.text = "\(arrCompanyNameContacts.count)"
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 0
                
            }
            else if indexPath.row == 2
            {
                cell.lblCount.text = "\(arrJobTitleContacts.count)"
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 0
            }
            else if indexPath.row == 3
            {
                cell.lblCount.text = "\(arrBirthdayContacts.count)"
                cell.mainView.clipsToBounds = true
                cell.mainView.layer.cornerRadius = 10
                cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner] // Bottom Corner
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("ContactHeaderView", owner: self, options: [:])?.first as! ContactHeaderView
        
        headerView.backgroundColor = UIColor.groupTableViewBackground
        
        headerView.lblName.text = arrHeaderName[section]
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllContactViewController") as! AllContactViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                
            }
        }
        else if indexPath.section == 1
        {
            if indexPath.row == 0
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicateContactVC") as! DuplicateContactVC
                vc.strTitle = "Duplicate Contacts"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicateContactVC") as! DuplicateContactVC
                vc.strTitle = "Duplicate Phones"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 2
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicateContactVC") as! DuplicateContactVC
                vc.strTitle = "Duplicate Emails"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 3
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DuplicateContactVC") as! DuplicateContactVC
                vc.strTitle = "Similar Names"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.section == 2
        {
            if indexPath.row == 0
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MissingInformationVC") as! MissingInformationVC
                vc.strTitle = "No Name"
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MissingInformationVC") as! MissingInformationVC
                vc.strTitle = "No Phones"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 2
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MissingInformationVC") as! MissingInformationVC
                vc.strTitle = "No Phones & Emails"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 3
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MissingInformationVC") as! MissingInformationVC
                vc.strTitle = "No Group"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.section == 3
        {
            if indexPath.row == 0
            {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
//                vc.strTitle = "Recently Addedd"
//                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                vc.strTitle = "Company"
                vc.arrFiltersContacts = self.arrCompanyNameContacts
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 2
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                vc.strTitle = "Job Title"
                vc.arrFiltersContacts = self.arrJobTitleContacts
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 3
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                vc.strTitle = "Birthday"
                vc.arrFiltersContacts = self.arrBirthdayContacts
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
