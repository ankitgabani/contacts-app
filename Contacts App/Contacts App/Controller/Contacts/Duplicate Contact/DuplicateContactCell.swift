//
//  DuplicateContactCell.swift
//  Contacts App
//
//  Created by Gabani King on 03/09/21.
//

import UIKit

class DuplicateContactCell: UITableViewCell {

    @IBOutlet weak var viewRound: UIView!
    @IBOutlet weak var lblFirstLater: UILabel!
    
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblSubTitleNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
