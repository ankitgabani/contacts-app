//
//  DuplicateContactView.swift
//  Contacts App
//
//  Created by Gabani King on 03/09/21.
//

import UIKit

class DuplicateContactView: UIView {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnMerge: UIButton!
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
