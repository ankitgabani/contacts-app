//
//  DuplicateContactVC.swift
//  Contacts App
//
//  Created by Gabani King on 03/09/21.
//

import UIKit
import Toast_Swift
class DuplicateContactVC: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var topRoundMiddleView: UIView!
    
    @IBOutlet weak var viewBlur: UIVisualEffectView!
    @IBOutlet weak var viewPopupMerge: UIView!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    
    var strTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBlur.isHidden = true
        
        lblMainTitle.text = strTitle
        print(strTitle)
        
        tblView.dataSource = self
        tblView.delegate = self
        
        topView.clipsToBounds = true
        topView.layer.cornerRadius = 25
        topView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- Action Method
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedMergeAllContact(_ sender: Any) {
        let alert = UIAlertController(title: "Merge Contact", message: "Are you sure you want to automatic merge ? You can't undo after merge.", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Merge", style: .cancel, handler: { action in
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
    }
    
    @IBAction func clickedCancel(_ sender: Any)
    {
        viewBlur.isHidden = true
    }
    
    @IBAction func clickedMerge(_ sender: Any)
    {
        viewBlur.isHidden = true
        self.view.makeToast("Contacts Merged Successfully.", duration: 2.5, position: .center)
    }
    
    @objc func clickedMergeContact(sender: UIButton)
    {
        viewBlur.isHidden = false


        self.viewPopupMerge.isHidden = false
        self.topRoundMiddleView.isHidden = false
        
        self.viewPopupMerge.alpha = 0.0
        self.topRoundMiddleView.alpha = 0.0
        self.viewPopupMerge.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        self.topRoundMiddleView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        
        UIView.animate(withDuration: 0.8, animations: {
            self.viewPopupMerge.transform = CGAffineTransform.identity
            self.topRoundMiddleView.transform = CGAffineTransform.identity
            
            self.viewPopupMerge.alpha = 1.0
            self.topRoundMiddleView.alpha = 1.0
        })
    }
    
    //MARK:- UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "DuplicateContactCell") as! DuplicateContactCell
        
        cell.viewRound.addGradientBackground()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("DuplicateContactView", owner: self, options: [:])?.first as! DuplicateContactView
        
        headerView.btnMerge.tag = section
        headerView.btnMerge.addTarget(self, action: #selector(clickedMergeContact(sender:)), for: .touchUpInside)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    
    
}

extension UIView {
    func addGradientBackground() {
        let random = UIColor.random().cgColor
        let random2 = UIColor.random().cgColor
        
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [random, random2]
        
        layer.insertSublayer(gradient, at: 0)
    }
}

extension UIColor {
    static func random() -> UIColor {
        let red = CGFloat.random(in: 0...1)
        let green = CGFloat.random(in: 0...1)
        let blue = CGFloat.random(in: 0...1)
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}

extension UIView {
    
    /// Remove UIBlurEffect from UIView
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
}
