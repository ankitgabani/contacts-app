//
//  ContactHomeTableCell.swift
//  Contacts App
//
//  Created by Gabani King on 26/08/21.
//

import UIKit

class ContactHomeTableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
