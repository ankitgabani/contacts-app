//
//  CommanClass.m
//  Contacts App
//
//  Created by Mac MIni M1 on 10/09/21.
//

#import "CommanClass.h"
#import <Contacts/Contacts.h>
#import <UIKit/UIKit.h>

@implementation CommanClass

-(void)contactsDetailsFromAddressBook{
    //ios 9+
    
    
    NSMutableArray *arrBirthday = [[NSMutableArray alloc] init];
    NSMutableArray *arrJobTitle = [[NSMutableArray alloc] init];
    NSMutableArray *arrCompanyTitle = [[NSMutableArray alloc] init];
    NSMutableArray *arrAllContacts = [[NSMutableArray alloc] init];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        CNContactStore *store = [[CNContactStore alloc] init];
        
        [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            
            if (granted == NO) {
                //show access denied popup
                return ;
            }
            __block NSString *phone;
            __block   NSString *fullName;
            __block   NSString *firstName;
            __block    NSString *lastName;
            __block    UIImage *profileImage;
            __block    NSDateComponents *birthDayComponent;
            __block     NSMutableArray *contactNumbersArray;
            __block    NSString *birthDayStr;
            __block     NSMutableArray *emailArray;
            __block     NSString* email = @"";
            __block     NSString* jobTitle = @"";
            __block     NSString* companyName = @"";
            
            NSError* contactError;
            CNContactStore* addressBook = [[CNContactStore alloc]init];
            [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
            NSArray *keysToFetch = @[CNContactMiddleNameKey,CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey,CNContactEmailAddressesKey,CNContactBirthdayKey,CNContactImageDataKey,CNContactJobTitleKey,CNContactOrganizationNameKey];
            CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
            
            [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){\
                
                contactNumbersArray = [[NSMutableArray alloc] init];
                
                firstName = contact.givenName;
                lastName = contact.familyName;
                birthDayComponent = contact.birthday;
                jobTitle = contact.jobTitle;
                companyName = contact.organizationName;
                
                if (lastName == nil) {
                    fullName=[NSString stringWithFormat:@"%@",firstName];
                }else if (firstName == nil){
                    fullName=[NSString stringWithFormat:@"%@",lastName];
                }
                else{
                    fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                }
                
                UIImage *image = [UIImage imageWithData:contact.imageData];
                if (image != nil) {
                    profileImage = image;
                }else{
                    profileImage = [UIImage imageNamed:@"placeholder.png"];
                }
                for (CNLabeledValue *label in contact.phoneNumbers) {
                    phone = [label.value stringValue];
                    if ([phone length] > 0) {
                        [contactNumbersArray addObject:phone];
                    }
                }
                ////Get all E-Mail addresses from contacts
                for (CNLabeledValue *label in contact.emailAddresses) {
                    email = label.value;
                    if ([email length] > 0) {
                        [emailArray addObject:email];
                    }
                }
                
                if (birthDayComponent == nil) {
                    // NSLog(@"Component: %@",birthDayComponent);
                    birthDayStr = @"DOB not available";
                }else{
                    birthDayComponent = contact.birthday;
                    NSInteger day = [birthDayComponent day];
                    NSInteger month = [birthDayComponent month];
                    NSInteger year = [birthDayComponent year];
                    // NSLog(@"Year: %ld, Month: %ld, Day: %ld",(long)year,(long)month,(long)day);
                    birthDayStr = [NSString stringWithFormat:@"%ld/%ld/%ld",(long)day,(long)month,(long)year];
                    //NSLog(@"EMAIL: %@",email);
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    
                    if (fullName != nil) {
                        [dic setValue:fullName forKey:@"fullName"];
                    }
                    if (profileImage != nil) {
                        [dic setValue:profileImage forKey:@"userImage"];
                    }
                    if (phone != nil) {
                        
                        NSString *strPhone = [contactNumbersArray componentsJoinedByString:@","];
                        [dic setValue:strPhone forKey:@"PhoneNumbers"];
                    }
                    if (birthDayStr != nil) {
                        [dic setValue:birthDayStr forKey:@"BirthDay"];
                    }
                    if (email != nil) {
                        [dic setValue:email forKey:@"userEmailId"];
                    }
                    
                    [arrBirthday addObject:dic];
                    
                }
                
                if (![jobTitle isEqualToString:@""]) {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    
                    if (fullName != nil) {
                        [dic setValue:fullName forKey:@"fullName"];
                    }
                    if (profileImage != nil) {
                        [dic setValue:profileImage forKey:@"userImage"];
                    }
                    if (phone != nil) {
                        
                        NSString *strPhone = [contactNumbersArray componentsJoinedByString:@","];
                        [dic setValue:strPhone forKey:@"PhoneNumbers"];
                    }
                    if (birthDayStr != nil) {
                        [dic setValue:birthDayStr forKey:@"BirthDay"];
                    }
                    if (email != nil) {
                        [dic setValue:email forKey:@"userEmailId"];
                    }
                    if (jobTitle != nil) {
                        [dic setValue:jobTitle forKey:@"jobTitle"];
                    }
                    
                    [arrJobTitle addObject:dic];
                }
                
                if (![companyName isEqualToString:@""]) {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    
                    if (fullName != nil) {
                        [dic setValue:fullName forKey:@"fullName"];
                    }
                    if (profileImage != nil) {
                        [dic setValue:profileImage forKey:@"userImage"];
                    }
                    if (phone != nil) {
                        
                        NSString *strPhone = [contactNumbersArray componentsJoinedByString:@","];
                        [dic setValue:strPhone forKey:@"PhoneNumbers"];
                    }
                    if (birthDayStr != nil) {
                        [dic setValue:birthDayStr forKey:@"BirthDay"];
                    }
                    if (email != nil) {
                        [dic setValue:email forKey:@"userEmailId"];
                    }
                    if (companyName != nil) {
                        [dic setValue:jobTitle forKey:@"companyName"];
                    }
                    
                    [arrCompanyTitle addObject:dic];
                }
                
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                
                if (fullName != nil) {
                    [dic setValue:fullName forKey:@"fullName"];
                }
                if (profileImage != nil) {
                    [dic setValue:profileImage forKey:@"userImage"];
                }
                if (phone != nil) {
                    
                    NSString *strPhone = [contactNumbersArray componentsJoinedByString:@","];
                    [dic setValue:strPhone forKey:@"PhoneNumbers"];
                }
                if (birthDayStr != nil) {
                    [dic setValue:birthDayStr forKey:@"BirthDay"];
                }
                if (email != nil) {
                    [dic setValue:email forKey:@"userEmailId"];
                }
                if (companyName != nil) {
                    [dic setValue:jobTitle forKey:@"companyName"];
                }
                
                [arrAllContacts addObject:contact];
                
            }];
            
        }];
        
        [self birthDayNotification:arrBirthday];
        [self jobTitleNotification:arrJobTitle];
        [self companyNotification:arrCompanyTitle];
        [self setupDuplicateContacts:arrAllContacts];
        
    });
    
}

-(void)birthDayNotification:(NSMutableArray *)array{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadBirthday" object:array userInfo:nil];
}

-(void)jobTitleNotification:(NSMutableArray *)array{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadJobTitle" object:array userInfo:nil];
}

-(void)companyNotification:(NSMutableArray *)array{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadCompanyName" object:array userInfo:nil];
}

-(void)setupDuplicateContacts:(NSMutableArray *)array
{
    //  NSMutableArray *arrObj = [self groupsWithDuplicatesRemoved:array myKeyParameter:@"fullName"];
    
}

- (NSMutableArray *) groupsWithDuplicatesRemoved:(NSArray *)  groups myKeyParameter:(NSString *)myKeyParameter {
    NSMutableArray * groupsFiltered = [[NSMutableArray alloc] init];    //This will be the array of groups you need
    NSMutableArray * groupNamesEncountered = [[NSMutableArray alloc] init]; //This is an array of group names seen so far
    NSMutableArray * duplicateContacts = [[NSMutableArray alloc] init]; //This is an array of group names seen so far
    
    NSString * name;        //Preallocation of group name
    for (NSDictionary * group in groups) {  //Iterate through all groups
        name = [NSString stringWithFormat:@"%@", [group objectForKey:myKeyParameter]]; //Get the group name
        if ([groupNamesEncountered indexOfObject: name]==NSNotFound) {  //Check if this group name hasn't been encountered before
            [groupNamesEncountered addObject:name]; //Now you've encountered it, so add it to the list of encountered names
            [groupsFiltered addObject:group];   //And add the group to the list, as this is the first time it's encountered
        }
        else{
            [duplicateContacts addObject:group];
        }
    }
    return duplicateContacts;
}

@end
