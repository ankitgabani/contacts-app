//
//  CommanClass.h
//  Contacts App
//
//  Created by Mac MIni M1 on 10/09/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommanClass : NSObject
{
    NSMutableArray *contactsArray;
}

-(void)contactsDetailsFromAddressBook;

@end

NS_ASSUME_NONNULL_END
